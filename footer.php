				</div><!-- end #main-content -->

				<footer id="main-footer">

					<div class="row">
						
						<div class="large-12 columns">

							<small>©<?php echo date('Y'); ?> <?php bloginfo( 'name' ); ?></small>
							
						</div>

					</div>

				</footer>

				<a class="exit-off-canvas"></a>

			</main>

		</div><!-- end .inner-wrap -->
	</div><!-- end .off-canvas-wrap -->

	

	<?php wp_footer(); ?>

</body>
</html>